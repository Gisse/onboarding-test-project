package com.payexpert.storage

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys

object SharedPreferencesManager {

    private const val PREFS_NAME = "encrypted_shared_prefs"
    private lateinit var prefs: SharedPreferences

    private fun initPreferences(context: Context): SharedPreferences {
        if (!::prefs.isInitialized || prefs == null) {
            prefs = EncryptedSharedPreferences.create(
                PREFS_NAME,
                MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
                context,
                EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            )
        }
        return prefs
    }

    /**
     * Get String value
     */
    fun getValue(context: Context, key: String, defaultValue: String) : String? {
        return initPreferences(context).getString(key, defaultValue)
    }

    /**
     * Get Int value
     */
    fun getValue(context: Context, key: String, defaultValue: Int) : Int{
        return initPreferences(context).getInt(key, defaultValue)
    }

    /**
     * Get Boolean value
     */
    fun getValue(context: Context, key: String, defaultValue: Boolean) : Boolean{
        return initPreferences(context).getBoolean(key, defaultValue)
    }

    /**
     * Get Long value
     */
    fun getValue(context: Context, key: String, defaultValue: Long) : Long{
        return initPreferences(context).getLong(key, defaultValue)
    }

    /**
     * Set String value
     */
    fun setValue(context: Context, key: String, value: String?){
        initPreferences(context).edit().putString(key, value).apply()
    }

    /**
     * Set Int value
     */
    fun setValue(context: Context, key: String, value: Int){
        initPreferences(context).edit().putInt(key, value).apply()
    }

    /**
     * Set Boolean value
     */
    fun setValue(context: Context, key: String, value: Boolean){
        initPreferences(context).edit().putBoolean(key, value).apply()
    }

    /**
     * Set Long value
     */
    fun setValue(context: Context, key: String, value: Long){
        initPreferences(context).edit().putLong(key, value).apply()
    }
}