package com.payexpert.extensions


import android.animation.ValueAnimator
import android.app.Activity
import android.graphics.Color
import android.os.Build
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.view.WindowManager

inline fun View.visibleIf(crossinline predicate:() -> Boolean) {
    if (predicate.invoke()) visible() else gone()
}

inline fun View.goneIf(crossinline predicate:() -> Boolean) {
    if (predicate.invoke()) gone() else visible()
}

fun View.visible() { visibility = VISIBLE }

fun View.invisible() {
    visibility = INVISIBLE
}

fun View.gone() { visibility = GONE }

fun View.enable() {
    isEnabled = true
}

fun View.enableIf(predicate: () -> Boolean) {
    isEnabled = predicate()
}

fun View.disable() {
    isEnabled = false
}

fun View.shake() {

//    val viewShake = ValueAnimator.ofInt(-7, 7, -7, 7, -5, 5, -3, 3, -1, 1, -1, 1, 0, 0, 0, 0)
    val viewShake = ValueAnimator.ofInt(-9, 9, -9, 9, -7, 7, -5, 5, -5, 5, -3, 3, -1, 1, -1, 1, 0, 0, 0, 0)
    viewShake.duration = 330
    viewShake.addUpdateListener { animation ->
        this.pivotX = (this.width / 2).toFloat()
        this.pivotY = this.height.toFloat()
        this.translationX = (animation.animatedValue as Int).toFloat()
    }

    viewShake.start()
}

fun Activity.makeStatusBarTransparent() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                            || View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            }
            statusBarColor = Color.TRANSPARENT
        }
    }
}

fun Activity.makeStatusBarWhite() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                            || View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            }
            statusBarColor = Color.WHITE
        }
    }
}

fun View.setMarginTop(marginTop: Int) {
    val menuLayoutParams = this.layoutParams as ViewGroup.MarginLayoutParams
    menuLayoutParams.setMargins(0, marginTop, 0, 0)
    this.layoutParams = menuLayoutParams
}