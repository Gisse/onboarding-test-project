package com.payexpert.extensions


import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import java.io.ByteArrayOutputStream
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


fun Int.dpToPx(): Int {
    return this * Resources.getSystem().displayMetrics.density.toInt()
}

fun String.pathToBase64(): String? {
    val bm = BitmapFactory.decodeFile(this)
    val baos = ByteArrayOutputStream()
    if (bm == null) return null
    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos)
    val b = baos.toByteArray()
    return Base64.encodeToString(b, Base64.DEFAULT)
}

fun Calendar.format(): String {
    val formatOut = SimpleDateFormat("MM/dd/yy", Locale.US)
    return formatOut.format(this.time)
}

fun Date.format(): String {
    val formatOut = SimpleDateFormat("dd MMM yyyy", Locale.US)
    return formatOut.format(TimeUnit.SECONDS.toMillis(this.time))
}

fun String.formatDate(): String {
    val formatIn = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
    val formatOut = SimpleDateFormat("MM/dd/yy", Locale.US)
    var date: Date? = null
    try {
        date = formatIn.parse(this)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return if (date != null) formatOut.format(date) else formatOut.format(Date())

}

fun String.getDate(): Date? {
    val formatIn = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
    var date: Date? = null
    try {
        date = formatIn.parse(this)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return date
}

inline fun <reified T : Enum<T>> Intent.putExtra(victim: T): Intent =
    putExtra(T::class.qualifiedName, victim.ordinal)

inline fun <reified T: Enum<T>> Intent.getEnumExtra(): T? =
    getIntExtra(T::class.qualifiedName, -1)
        .takeUnless { it == -1 }
        ?.let { T::class.java.enumConstants?.get(it) }