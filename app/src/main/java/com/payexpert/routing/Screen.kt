package com.payexpert.routing

import com.payexpert.presentation.onboarding.OnboardingActivity
import com.payexpert.presentation.userverification.SignInActivity
import com.payexpert.presentation.splash.SplashActivity
import com.payexpert.presentation.userverification.SignUpActivity

enum class Screen {

    SPLASH,
    ONBOARDING,
    SIGN_IN,
    SIGN_UP;

    fun getScreenClass(): Class<*> {
        return when (this) {
            SPLASH -> SplashActivity::class.java
            ONBOARDING -> OnboardingActivity::class.java
            SIGN_IN -> SignInActivity::class.java
            SIGN_UP -> SignUpActivity::class.java
        }
    }
}