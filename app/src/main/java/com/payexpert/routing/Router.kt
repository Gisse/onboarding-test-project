package com.payexpert.routing

import android.content.Context
import android.content.Intent
import android.os.Bundle

object Router {
    fun goTo(screen: Screen, context: Context, singleTop: Boolean = false, clearTop: Boolean = false, clearTask: Boolean = false, newTask: Boolean = false) {
        val intent = Intent(context, screen.getScreenClass())
        intent.apply {
            if (singleTop) {
                addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            }
            if (clearTop) {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            }
            if (clearTask) {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            }
            if (newTask) {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
        }
        context.startActivity(intent)
    }

    fun goToWithFlag(screen: Screen, context: Context) {
        val intent = Intent(context, screen.getScreenClass())
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(intent)
    }

    fun goToWithFlagAndBundle(screen: Screen, bundle: Bundle, context: Context) {
        val intent = Intent(context, screen.getScreenClass())
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtras(bundle)
        context.startActivity(intent)
    }
}