package com.payexpert.presentation.splash

import android.os.Bundle
import com.crashlytics.android.Crashlytics
import com.payexpert.R
import com.payexpert.extensions.ioToMain
import com.payexpert.extensions.makeStatusBarTransparent
import com.payexpert.presentation.base.BaseActivity
import com.payexpert.routing.Router
import com.payexpert.routing.Screen
import io.fabric.sdk.android.Fabric
import io.reactivex.Single
import java.util.concurrent.TimeUnit

class SplashActivity : BaseActivity() {

    companion object {
        const val SPLASH_DELAY_MS = 2000L
    }

    val TAG = "SplashActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        makeStatusBarTransparent()
//        if (!BuildConfig.DEBUG) {
//        Fabric.with(this, Crashlytics())
//        }
        startTimer()
    }

    private fun startTimer() {
        disposable.add(
            Single.timer(
                SPLASH_DELAY_MS,
                TimeUnit.MILLISECONDS
            ).ioToMain().subscribe({
                Router.goTo(Screen.ONBOARDING, this, true, true, true, true)
                finish()
            }, {
                Router.goTo(Screen.ONBOARDING, this, true, true, true, true)
                finish()
            })
        )
    }
}