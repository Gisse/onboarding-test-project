package com.payexpert.presentation.dialog

import android.app.Activity
import android.content.Intent

class DialogHelper {

    companion object {

        const val DURATION = "duration"

        fun showPreStartRideDialog(activity: Activity, requestCode : Int) {
            val intent = Intent(activity, TermsAndConditionsDialog::class.java)
            activity.startActivityForResult(intent, requestCode)
        }
    }
}