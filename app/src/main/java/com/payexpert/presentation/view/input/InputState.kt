package com.payexpert.presentation.view.input

sealed class InputState
object Valid : InputState()
object Empty : InputState()
object Invalid : InputState()
data class Short(val min: Int) : InputState()