package com.payexpert.presentation.view.input

import io.reactivex.Observable
import java.util.regex.Pattern

interface InputValidator {
    fun validate(input: String): InputState
}

class EmptyInputValidator : InputValidator {

    override fun validate(input: String) = if (input.isNotEmpty()) Valid else Empty
}

class PasswordInputValidator(private val min: Int = 4) : InputValidator {

    override fun validate(input: String) = if (input.length < min) Invalid else Valid
}

class EmailInputValidator : InputValidator {

    override fun validate(input: String) = if (input.isNotEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(input).matches()) Valid else Invalid
}

class TransportPinInputValidator(private val min: Int = 4, private val max: Int = 6) : InputValidator {

    override fun validate(input: String) = if (input.length in (min + 1) until max) Invalid else Valid
}

class AmountInputValidator : InputValidator {
    private val pattern = Pattern.compile("^\\d+(\\.\\d{1,3})?$")
    override fun validate(input: String): InputState = if (input.isNotEmpty() && pattern.matcher(input).matches()) Valid else Invalid
}

fun Observable<String>.validateInput(inputValidator: InputValidator): Observable<InputState> = map { inputValidator.validate(it) }