package com.payexpert.presentation.view.input

import java.util.regex.Pattern

interface PinInputValidator {
    fun validate(input: String): TransportPinInputState
}

class EmptyPinInputValidator : PinInputValidator {
    override fun validate(input: String) = if (input.isNotEmpty()) TransportPinInputState.VALID else TransportPinInputState.EMPTY
}

class LengthPinInputValidator(private val min: Int = 4, private val max: Int = 6) : PinInputValidator {
    override fun validate(input: String) = if (input.length in min until max + 1) TransportPinInputState.VALID else TransportPinInputState.WRONG_LENGTH
}

class CharsPinInputValidator() : PinInputValidator {

    private val pattern = Pattern.compile("^[a-zA-Z0-9]+$")

    override fun validate(input: String) = if (pattern.matcher(input).matches()) TransportPinInputState.VALID else TransportPinInputState.WRONG_CHARS
}