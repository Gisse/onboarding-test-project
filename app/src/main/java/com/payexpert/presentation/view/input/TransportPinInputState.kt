package com.payexpert.presentation.view.input

import com.payexpert.R

enum class TransportPinInputState {

    VALID, EMPTY, WRONG_LENGTH, WRONG_CHARS;

    fun getErrorMessageRes(): Int {
        return when (this) {
            VALID -> -1
            EMPTY -> -1
            WRONG_LENGTH -> -1
            WRONG_CHARS -> -1
        }
    }

}