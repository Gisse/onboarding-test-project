package com.payexpert.presentation.utils

import com.payexpert.R

enum class PESpanableStyle {

    TERMS_OF_USE {

        override fun getTextRes(): Int {
            return R.string.i_m_accepting_bigx_terms_of_use
        }

        override fun getSpanStart(language: String): Int {
            return when (language) {
                "en" -> 19
                "ru" -> 19
                "de" -> 19
                else -> 19
            }
        }

        override fun getSpanEnd(language: String): Int {
            return when (language) {
                "en" -> 31
                "ru" -> 31
                "de" -> 31
                else -> 31
            }
        }
    },
    GDPR {
        override fun getTextRes(): Int {
            return R.string.i_m_allowing_bigx_to_collect_my_data_gdpr_agreement
        }
        override fun getSpanStart(language: String): Int {
            return when (language) {
                "en" -> 37
                "ru" -> 37
                "de" -> 37
                else -> 37
            }
        }

        override fun getSpanEnd(language: String): Int {
            return when (language) {
                "en" -> 54
                "ru" -> 54
                "de" -> 54
                else -> 54
            }
        }
    };

    open fun isUnderlineText(): Boolean = true
    open fun isFakeBoldText(): Boolean = true
    open fun getColorRes(): Int = R.color.black
    open fun getTextRes(): Int = -1
    open fun getSpanStart(language: String): Int = -1
    open fun getSpanEnd(language: String): Int = -1
    open fun getIconRes(): Int = -1
}