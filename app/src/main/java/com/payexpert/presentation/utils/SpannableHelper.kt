package com.payexpert.presentation.utils

import android.graphics.Color
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.TextView
import java.util.*

class SpannableHelper {

    companion object {
        fun addSpan(textView: TextView, text: String, spanStyle: PESpanableStyle, language: String = Locale.getDefault().language, spanClickHandler: () -> Unit) {

            var start = spanStyle.getSpanStart(language)
            var end = spanStyle.getSpanEnd(language)

            if (start > text.length) {
                return
            }

            if (end > text.length) {
                end = text.length
            }

            val ss = SpannableString(text)
            val clickableSpan = object : ClickableSpan() {
                override fun onClick(textView: View) {
                    spanClickHandler.invoke()
                }

                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.isUnderlineText = spanStyle.isUnderlineText()
                    ds.isFakeBoldText = spanStyle.isFakeBoldText()
                    ds.color = textView.context.getColor(spanStyle.getColorRes())
                }
            }
            ss.setSpan(clickableSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

            textView.text = ss
            textView.movementMethod = LinkMovementMethod.getInstance()
            textView.highlightColor = Color.TRANSPARENT
        }
    }
}