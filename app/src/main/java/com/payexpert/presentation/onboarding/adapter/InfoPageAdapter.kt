package com.payexpert.presentation.onboarding.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.payexpert.R
import com.payexpert.extensions.gone
import com.payexpert.extensions.visible
import com.payexpert.presentation.onboarding.OnboardingActivity
import com.payexpert.presentation.onboarding.OnbordingInfo

class InfoPageAdapter(
    val layoutInflater: LayoutInflater,
    val infoList: List<OnbordingInfo>,
    val onboardingActivity: OnboardingActivity
): PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = layoutInflater.inflate(R.layout.onboarding_info_layout, container, false)
        val title: TextView = view.findViewById(R.id.onboardingInfoTitle)
        val text: TextView = view.findViewById(R.id.onboardingInfoText)
        val onboardingSignIn: TextView = view.findViewById(R.id.onboardingSignIn)
        val onboardingSignUp: TextView = view.findViewById(R.id.onboardingSignUp)
        val onboardingSignInAditional: TextView = view.findViewById(R.id.onboardingSignInAditional)
        val learnMore: Button = view.findViewById(R.id.onboardingLearnMore)
        val onboardingSignUpButton: Button = view.findViewById(R.id.onboardingSignUpButton)

        when (position) {
            0 -> {
                onboardingSignIn.visible()
                onboardingSignUp.visible()
                learnMore.visible()
                onboardingSignInAditional.gone()
                onboardingSignUpButton.gone()
                learnMore.setOnClickListener{
                    onboardingActivity.learnMore()
                }
                onboardingSignIn.setOnClickListener{
                    onboardingActivity.signIn()
                }
                onboardingSignUp.setOnClickListener{
                    onboardingActivity.signUp()
                }
            }
            1 -> {
                onboardingSignIn.gone()
                onboardingSignUp.gone()
                learnMore.gone()
                onboardingSignInAditional.gone()
                onboardingSignUpButton.gone()
            }
            2 -> {
                onboardingSignIn.gone()
                onboardingSignUp.gone()
                learnMore.gone()
                onboardingSignInAditional.gone()
                onboardingSignUpButton.gone()
            }
            3 -> {
                onboardingSignIn.gone()
                onboardingSignUp.gone()
                learnMore.gone()
                onboardingSignInAditional.visible()
                onboardingSignUpButton.visible()
                onboardingSignInAditional.setOnClickListener{
                    onboardingActivity.signIn()
                }
                onboardingSignUpButton.setOnClickListener{
                    onboardingActivity.signUp()
                }
            }
        }

        val onboardingInfo = infoList[position]
        title.text = onboardingInfo.onboardingTitle
        text.text = onboardingInfo.onboardingText

        container.addView(view)
        return view
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return infoList.size
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

    interface OnboardingAdapterInterface{
        fun learnMore()
        fun signIn()
        fun signUp()
    }
}