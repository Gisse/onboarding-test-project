package com.payexpert.presentation.onboarding

import android.os.Bundle
import android.util.Log
import androidx.viewpager.widget.ViewPager
import com.payexpert.R
import com.payexpert.extensions.dpToPx
import com.payexpert.extensions.makeStatusBarTransparent
import com.payexpert.presentation.base.BaseActivity
import com.payexpert.presentation.onboarding.adapter.ImagePageAdapter
import com.payexpert.presentation.onboarding.adapter.InfoPageAdapter
import com.payexpert.routing.Router
import com.payexpert.routing.Screen
import kotlinx.android.synthetic.main.activity_onboarding.*

class OnboardingActivity : BaseActivity(), InfoPageAdapter.OnboardingAdapterInterface {

    val TAG = "OnboardingActivity"

    var pagerPosition = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)
        makeStatusBarTransparent()
        initPager()
        syncPagers()
    }

    private fun syncPagers() {
        onboardingImageAdapter.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            private var mScrollState = ViewPager.SCROLL_STATE_IDLE

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                if (mScrollState == ViewPager.SCROLL_STATE_IDLE) {
                    return
                }
                onboardingInfoPageAdapter.scrollTo(
                    onboardingImageAdapter.getScrollX(),
                    onboardingInfoPageAdapter.getScrollY()
                )
            }

            override fun onPageSelected(position: Int) {
                pagerPosition = position
            }

            override fun onPageScrollStateChanged(state: Int) {
                mScrollState = state
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    onboardingInfoPageAdapter.setCurrentItem(
                        onboardingImageAdapter.getCurrentItem(),
                        true
                    )
                }
            }
        }
        )

        onboardingInfoPageAdapter.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            private var mScrollState = ViewPager.SCROLL_STATE_IDLE

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                if (mScrollState == ViewPager.SCROLL_STATE_IDLE) {
                    return
                }
                onboardingImageAdapter.scrollTo(
                    onboardingInfoPageAdapter.getScrollX(),
                    onboardingImageAdapter.getScrollY()
                )
            }

            override fun onPageSelected(position: Int) {
                pagerPosition = position
            }

            override fun onPageScrollStateChanged(state: Int) {
                mScrollState = state
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    onboardingImageAdapter.setCurrentItem(
                        onboardingInfoPageAdapter.getCurrentItem(),
                        true
                    )
                }
            }
        }
        )
    }

    private fun initPager() {
        onboardingInfoPageAdapter.adapter = InfoPageAdapter(layoutInflater, preparePackages(), this)
        onboardingInfoPageAdapter.clipToPadding = false
        onboardingInfoPageAdapter.pageMargin = 20.dpToPx()

        onboardingImageAdapter.adapter =
            ImagePageAdapter(
                layoutInflater,
                prepareImages()
            )
        onboardingImageAdapter.clipToPadding = false
        onboardingImageAdapter.pageMargin = 20.dpToPx()
        onboardingCircleIndicator.setViewPager(onboardingImageAdapter)
    }

    private fun prepareImages(): List<Int> {
        return listOf(R.drawable.logo, R.drawable.logo, R.drawable.logo, R.drawable.logo)
    }

    private fun preparePackages(): List<OnbordingInfo> {
        val onboardingObject1 = OnbordingInfo(
            getString(R.string.onboarding_text_1),
            getString(R.string.onboarding_text_1_title)
        )
        val onboardingObject2 = OnbordingInfo(
            getString(R.string.onboarding_text_2),
            getString(R.string.onboarding_text_2_title)
        )
        val onboardingObject3 = OnbordingInfo(
            getString(R.string.onboarding_text_3),
            getString(R.string.onboarding_text_3_title)
        )
        val onboardingObject4 = OnbordingInfo(
            getString(R.string.onboarding_text_4),
            getString(R.string.onboarding_text_4_title)
        )
        return listOf(onboardingObject1, onboardingObject2, onboardingObject3, onboardingObject4)
    }

    override fun learnMore() {
        onboardingImageAdapter.setCurrentItem(1, true)
    }

    override fun signIn() {
        Router.goTo(Screen.SIGN_IN, this, true, true, true, true)
        finish()
    }

    override fun signUp() {
        Router.goToWithFlag(Screen.SIGN_UP, applicationContext)
    }
}