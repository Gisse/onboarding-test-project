package com.payexpert.presentation.onboarding.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.payexpert.R

class ImagePageAdapter(val layoutInflater: LayoutInflater, val imageList: List<Int>): PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = layoutInflater.inflate(R.layout.onboarding_image_layout, container, false)
        val onboardingImage: ImageView = view.findViewById(R.id.onboardingImage)

        val onboardingImageResource = imageList[position]

        onboardingImage.setImageResource(onboardingImageResource)

        container.addView(view)
        return view
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return imageList.size
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }
}