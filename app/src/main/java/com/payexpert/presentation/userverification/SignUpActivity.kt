package com.payexpert.presentation.userverification

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.payexpert.R
import com.payexpert.extensions.gone
import com.payexpert.extensions.makeStatusBarTransparent
import com.payexpert.extensions.visible
import com.payexpert.presentation.base.BaseActivity
import com.payexpert.presentation.dialog.DialogHelper
import com.payexpert.presentation.utils.PESpanableStyle
import com.payexpert.presentation.utils.SpannableHelper
import kotlinx.android.synthetic.main.activity_sign_up_layout.*
import java.util.*

class SignUpActivity : BaseActivity() {

    var thermsChecked = false
    var gdprChecked = false
    var numberFilled = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_layout)
        makeStatusBarTransparent()
        preparePhoneEditText()
        prepareClickListeners()
        prepareSpans()
    }

    private fun prepareSpans() {
        SpannableHelper.addSpan(termsText, getString(PESpanableStyle.TERMS_OF_USE.getTextRes()), PESpanableStyle.TERMS_OF_USE, Locale.getDefault().language) {
            DialogHelper.showPreStartRideDialog(this, 1)
            Log.d("djevtic", "Span clicked")
        }

        SpannableHelper.addSpan(gdprText, getString(PESpanableStyle.GDPR.getTextRes()), PESpanableStyle.GDPR, Locale.getDefault().language) {
            //            val browserIntent = Intent(Intent.ACTION_VIEW)
//            browserIntent.data = Uri.parse(getString(R.string.gdpr))
//            startActivity(browserIntent)
            Log.d("djevtic", "Span clicked GDPR")
        }
    }

    private fun preparePhoneEditText() {
        signUpButton.isEnabled = false
        countryCodePicker.registerCarrierNumberEditText(carrierNumber)
        countryCodePicker.setPhoneNumberValidityChangeListener {
            if(it) {
                clearNumber.visible()
            } else {
                clearNumber.gone()
            }
            numberFilled = it
            checkIsButtonEnabled()
        }
    }

    private fun prepareClickListeners() {
        toggleTearms.setOnClickListener {
            thermsChecked = !thermsChecked
            toggleTearms.isChecked = thermsChecked
            checkIsButtonEnabled()
        }

        gdprTearms.setOnClickListener {
            gdprChecked = !gdprChecked
            gdprTearms.isChecked = gdprChecked
            checkIsButtonEnabled()
        }

        backArrow.setOnClickListener {
            onBackPressed()
        }

        clearNumber.setOnClickListener {
            clearNumber.gone()
            carrierNumber.text.clear()
        }
    }

    private fun checkIsButtonEnabled() {
        signUpButton.isEnabled = thermsChecked && gdprChecked && numberFilled
    }
}