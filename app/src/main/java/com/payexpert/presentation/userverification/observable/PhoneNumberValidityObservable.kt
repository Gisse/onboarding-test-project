/********************************************************************************
 *	Copyright © 2019 Sinskit. All rights reserved.
 *	----------------------------------------------------------------------------
 *	Payexpert Source code.
 *
 *	File: PhoneNumberValidityObservable.kt
 *	Description:
 *
 *	History:
 *	- 13.01.2020: Created by Andrew Lasinskiy
 ********************************************************************************/
package com.payexpert.presentation.userverification.observable

import android.widget.EditText
import com.hbb20.CountryCodePicker
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.MainThreadDisposable

fun CountryCodePicker.validChanges(carrierNumber: EditText): Observable<Boolean> {
    return PhoneNumberValidityObservable(this, carrierNumber)
}

class PhoneNumberValidityObservable(
    private val countryCodePicker: CountryCodePicker,
    private val carrierNumber: EditText
): Observable<Boolean>() {

    override fun subscribeActual(observer: Observer<in Boolean>) {
//        if (!checkMainThread(observer)) {
//            return
//        }
        val listener = Listener(countryCodePicker, observer)
        observer.onSubscribe(listener)
        countryCodePicker.registerCarrierNumberEditText(carrierNumber)
        countryCodePicker.setPhoneNumberValidityChangeListener(listener)
    }

    private class Listener(
        private val countryCodePicker: CountryCodePicker,
        private val observer: Observer<in Boolean>
    ): MainThreadDisposable(), CountryCodePicker.PhoneNumberValidityChangeListener {
        override fun onValidityChanged(isValidNumber: Boolean) {
            if (!isDisposed) {
                observer.onNext(isValidNumber)
            }
        }

        override fun onDispose() {
            countryCodePicker.setPhoneNumberValidityChangeListener(null)
        }
    }

}