package com.payexpert.presentation.userverification

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import com.jakewharton.rxbinding3.view.focusChanges
import com.jakewharton.rxbinding3.view.touches
import com.jakewharton.rxbinding3.widget.textChanges
import com.payexpert.R
import com.payexpert.extensions.*
import com.payexpert.presentation.base.BaseActivity
import com.payexpert.presentation.userverification.observable.validChanges
import com.payexpert.presentation.view.input.PasswordInputValidator
import com.payexpert.presentation.view.input.Valid
import com.payexpert.routing.Router
import com.payexpert.routing.Screen
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import kotlinx.android.synthetic.main.activity_sign_in_layout.*
import java.util.concurrent.TimeUnit

class SignInActivity : BaseActivity() {

    private val phoneNumberChange: Observable<String> by lazy { carrierNumber.textChanges().map { it.toString() } }
    private val passwordTouch: Observable<MotionEvent> by lazy { showPassword.touches() }
    private val passwordChange: Observable<String> by lazy { password.textChanges().map { it.toString() }  }

    private val phoneNumberFocusChange: Observable<Boolean> by lazy { carrierNumber.focusChanges() }
    private val passwordFocusChange: Observable<Boolean> by lazy { password.focusChanges() }

    private val phoneValidityListener: Observable<Boolean> by lazy { countryCodePicker.validChanges(carrierNumber) }

    private val passwordInputValidator = PasswordInputValidator()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in_layout)
        makeStatusBarTransparent()
        preparePasswordListener()
    }

    private fun preparePasswordListener() {
        disposable.add(passwordTouch.subscribe {
            when (it.action) {
                MotionEvent.ACTION_DOWN -> {
                    password.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    password.setSelection(password.text.length)
                }
                MotionEvent.ACTION_UP -> {
                    password.transformationMethod = PasswordTransformationMethod.getInstance()
                    password.setSelection(password.text.length)
                }
            }
        })
        disposable.add(Observable.combineLatest(
            phoneValidityListener,
            passwordChange,
            fieldValidator()
        ).throttleWithTimeout(300L, TimeUnit.MILLISECONDS).startWith(false).observeMain().subscribe {
            signInButton?.enableIf { it }
        })
        disposable.add(phoneNumberChange.subscribe{
            clearNumber.visibleIf { it.isNotEmpty() }
        })
//        disposable.add(phoneNumberFocusChange.subscribe {
//            if (it) {
//                phonePicker.requestFocus()
//            } else {
//                phonePicker.clearFocus()
//            }
//        })
        signUpButton.setOnClickListener {
            Router.goToWithFlag(Screen.SIGN_UP, applicationContext)
        }

        clearNumber.setOnClickListener {
            clearNumber.gone()
            carrierNumber.text.clear()
        }

    }

    private fun fieldValidator() = BiFunction<Boolean, String, Boolean> { isPhoneValid, password ->
        isPhoneValid && passwordInputValidator.validate(password) == Valid
    }

}